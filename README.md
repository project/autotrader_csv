# Autotrader CSV

## Architectural Plan

1. Create a service that creates a CSV.
2. Plug the service into Drush, Console, and Cron
3. Make as much of this configurable as we can in the administrative interface
4. If budget allows, come up with some way to create/store "queries" that bring in specific entities (maybe provide a view?) that will be used for creating the CSV. If no budget, make it an API that can be extended/altered easily.

## Documentation on how to use.

TBD.